<?php

// Ceci est un fichier langue de SPIP -- This is a SPIP language file

// Fichier produit par PlugOnet
// Module: paquet-daterubriques
// Langue: fr
// Date: 09-06-2016 10:19:13
// Items: 3

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// D
	'daterubriques_description' => 'Ajoute un champ "date" sur les rubriques de SPIP gérable par le rédacteur (le champ date par défaut de spip étant modifié automatiquement lors des mises à jour des articles contenus dans les rubriques)',
	'daterubriques_nom' => 'Date pour Rubriques',
	'daterubriques_slogan' => 'Prendre la main sur le champ date des rubriques',
);
?>