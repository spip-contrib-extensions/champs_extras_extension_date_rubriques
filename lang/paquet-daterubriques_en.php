<?php

// Ceci est un fichier langue de SPIP -- This is a SPIP language file

// Fichier produit par PlugOnet
// Module: paquet-daterubriques
// Langue: en
// Date: 09-06-2016 10:19:13
// Items: 3

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// D
	'daterubriques_description' => 'Add a "date" field for SPIP sections manageable by the author (the default date field is modified automatically when there is updates of the articles)',
	'daterubriques_nom' => 'Date for Sections',
	'daterubriques_slogan' => 'Add a "date_utile" field for SPIP sections',
);
?>